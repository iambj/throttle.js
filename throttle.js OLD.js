// General purpose throttler function.
//
//      Author:     Brandon Johnson
//      Version:    0.0.2b
//      Liscense:   MIT (do whatever you want!)
//_____________________________________
//
//      time (num)     - in milliseconds, required between consecutive function calls
//      scope (string) - allows for mulitple things to be throttled by placing them in seperate "holders"
//      callback       - functiong to be called if it is not throttled
//      args           - arguments passed into the callback function (Might need new idea)
//                       args is passed back to your function as a list of parameters. Access them by args[i] in your function.
//
//      Returns FALSE if the execution is being throttled,
//      TRUE if the it continues as normal, and NULL when
//      throttling is over;
//
var throttled = []; //global holding cell for scope.
var throttle = function(time, scope, callback, args){
    if(throttled.length >= 1){
        for(var i = 0; i <= throttle.length; i++){
            if(throttled[i] === scope){
                return false;
            }
        }
    }
    throttled.push(scope);
    var timer = setTimeout(function () {
        throttled.splice(throttled.indexOf(scope, 1));
        }, time);
        if(typeof callback === "function"){
            callback(args);
        }
        return null;
    }, time);
    return true;
};



//for testing!
var testme = function(){
    throttle(2000, false, function(){
        document.getElementById('test').innerHTML += "Throttttttling!";
    });
};

/*
var halted = []; coming soon!
var halt = function(){

}
*/