### Throttle.js `v.0.0.3`
by Brandon Johnson
____

Throttle.js is a simple solution I always wanted for controlling how fast users can repeatedly click things. This tries to wrap those functions and only allow execution once every so many seconds. 


###### Usage: 
`throttle(time, scope, callback, args[array])`

(Where `time` is the delay until the next call will be allowed, `scope` allows groupings of throttling, `callback` is the function to call, `args[array]` takes a list of parameters to pass your function.

___

### TODO:
- Fix args array
- `stopProc()` function pluginish thing to disable multiple/duplicate behaviors. (i.e. don't open an already open window)
- Figure out if the test has multiple loops called? _(Does it really matter though for the demonstration?)_

### Change log:

February 6, 2015:

- Fixed a blocking error in the program flow (I think)

April 10, 2014:

- Initial creation.